# WindStore

Un site web en HTML/CSS/JS réalisé à l'occasion d'un projet lors de ma première année de licence en informatique (2018). Il s'agit de mon premier site web.

![WindStore cover](./public/img/cover.png)

## À propos du projet

- **Nature :** projet universitaire
- **Date :** avril 2018
- **Description :** il s'agit d'un site web d'un logiciel (fictif) nommé WindStore, permettant sa présentation et son téléchargement. WindStore est un logiciel permettant de télécharger et installer d’autres logiciels de manière sécurisée (lien officiel, dernière version, etc.). Ce dernier fonctionne exactement comme un Google Play Store, un AppStore ou même un Windows Store. Il suffit simplement d’entrer le nom du logiciel souhaité dans la barre de rechercher puis de cliquer sur « installer »

## Technologies utilisées

- HTML
- CSS
- JS

## En savoir plus

Pour en savoir plus, un PDF nommé `compte_rendu.pdf` est disponible dans ce répertoire.