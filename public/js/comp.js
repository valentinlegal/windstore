document.addEventListener('DOMContentLoaded', init, false);

function init() {
  // Get the checkbox
  var oui = document.getElementById("oui");
  // Get the output text
  var reponseOui = document.getElementById("reponseOui");

  // If the checkbox is checked, display the output text
  if (oui.checked == true){
    reponseOui.style.display = "block";
  } else {
    reponseOui.style.display = "none";
  }

    var non = document.getElementById("non");
  // Get the output text
  var reponseNon = document.getElementById("reponseNon");

  // If the checkbox is checked, display the output text
  if (non.checked == true){
    reponseNon.style.display = "block";
  } else {
    reponseNon.style.display = "none";
  }
    
} 